#!/usr/bin/env chez --script
(import (compose))
(assert ((compose odd? +) 1 2))
(assert ((compose not even?) 1))
