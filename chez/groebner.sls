#!r6rs
(library (groebner)
  (export groebner groebner+opt)
  (import (except (rnrs) lcm)
	  (polynomial)
	  (monomial)
	  (term)
	  (compose)
	  (ordering)
	  (rref)
	  (util)
	  (division))
  
  (define (lcm m n)
    (vector-map max m n))
  
  (define (syxyzy f g)
    (let* ([t (monomial->term (lcm (lm f) (lm g)))]
	   [p (term->polynomial (t/ t (lt f)))]
	   [q (term->polynomial (t/ t (lt g)))])
      (p- (p* p f) (p* q g))))

  (define (minimalize . g)
    (let loop ([fut g] [pat '()])
      (if (null? fut) pat
	  (let ([dividend (term->polynomial (lt (car fut)))]
		[divisors (map (compose term->polynomial lt) (append (cdr fut) pat))])
	    (if (null? (apply remainder  dividend divisors))
		(loop (cdr fut) pat)
		(loop (cdr fut) (cons (car fut) pat)))))))

  (define (reduce . g)
    (define (list->matrix l)
      (list->vector (map list->vector l)))
    (define (matrix->list m)
      (vector->list (vector-map vector->list m)))
    (let* ([mon (unique (apply append (map (lambda (p) (map car p)) g)))]
	   [mat (map (lambda (p) (map (lambda (m) (cdr (or (assoc m p) (cons m 0)))) mon)) g)]
	   [pol (map (lambda (p) (remp (lambda (t) (zero? (cdr t))) (map cons mon p)))
		     (matrix->list (rref! (list->matrix mat))))])
      pol))
  
  (define (groebner . f)
    (define (proc p q)
      (or (p= p q) (apply remainder (syxyzy p q) f)))
    (define (pred p)
      (or (not (pair? p)) (memp (lambda (q) (p= p q)) f)))
    (let ([rs (unique (remp pred (map-combination proc f f)))])
      (if (zero? (length rs))
	  (apply reduce (apply minimalize f))
	  (apply groebner (append f rs)))))
  
  (define (groebner+opt . f)
    (define (base p q f)
      (or (p= p q)
	  (m= (m* (lm p) (lm q)) (lcm (lm p) (lm q)))
	  (apply remainder (syxyzy p q) f)))
    (define (basis f g)
      (if (null? g) f
	  (let* ([h (append f g)]
		 [pred (lambda (p) (or (not (pair? p)) (memp (lambda (q) (p= p q)) h)))]
		 [basis1 (map-combination base f g (list h))]
		 [basis2 (map-combination base g g (list h))]
		 [basis3 (unique (remp pred (append basis1 basis2)))])
	    (basis h basis3))))
    (let* ([pred (lambda (p) (or (not (pair? p)) (memp (lambda (q) (p= p q)) f)))]
	   [basis1 (map-combination base f f (list f))]
	   [basis2 (unique (remp pred basis1))])
      (apply reduce (apply minimalize (basis f basis2)))))
  )

