#!r6rs
(library (printer)
  (export print print-tex)
  (import (chezscheme)
	  (variables)
	  (regex))

  (define (print-1 t)
    (let* ([a (cdr t)]
	   [x (car t)]
	   [b (map zero? (vector->list x))]
	   [c (string->list (variables))]
	   [d (vector->list x)]
	   [e (apply append (map list b c d))])
      (format "~:[~;+~]~a~{~:[*~a^~a~;~2*~]~}" (positive? a) a e)))
  
  (define (print q)
    (let* ([s1 (apply string-append (map print-1 q))]
	   [s2 (regex-replace-all s1 "\\^1([*+-])" "$1")]
	   [s3 (regex-replace-all s2 "([+-])1\\*" "$1")]
	   [s4 (regex-replace-all s3 "^\\+" "")])
      (regex-replace-all s4 "\\^1$" "")))
  
  (define (print-tex q)
    (let* ([s1 (print q)]
	   [s2 (regex-replace-all s1 "\\^([0-9]+)" "^{$1}")]
	   [s3 (regex-replace-all s2 "([0-9]+)/([0-9]+)" "\\frac{$1}{$2}")])
      (regex-replace-all s3 "\\*" "")))
  )
