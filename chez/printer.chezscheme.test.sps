#!/usr/bin/env chez --script

(import (parser) (printer))

(assert (equal? (print (parse "x+y")) "x+y"))
