#!/usr/bin/env chez --script
(import (term) (variables))
(set-variables! "xyz")
(assert (t= (t* '(#(1 0 0) . 1) '(#(1 0 0) . 1)) '(#(2 0 0) . 1)))
(assert (t= (t/ '(#(1 0 0) . 1) '(#(1 0 0) . 1)) '(#(0 0 0) . 1)))
	    
