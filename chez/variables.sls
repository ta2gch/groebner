#!r6rs
(library (variables)
  (export variables set-variables!)
  (import (rnrs base))
  (define vars "abcdefghijklmnopqrstuvwxyz")
  (define (variables) vars)
  (define (set-variables! variables) (set! vars variables))
  )
