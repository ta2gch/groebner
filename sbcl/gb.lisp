(defstruct trie value children)

(defun gettrie (key trie &optional default)
  (labels ((gettrie-1 (trie key)
	     (cond ((not trie) default)
		   ((null key) (trie-value trie))
		   (t (gettrie-1
		       (rest (assoc (first key) (trie-children trie)))
		       (rest key))))))
    (gettrie-1 trie key)))

(defun (setf gettrie) (value key trie)
  (labels ((setf-gettrie-1 (v k n)
	     (setf (trie-children n)
		   (acons k (make-trie :value v)
			  (delete k (trie-children n) :key #'first)))))
    (destructuring-bind (e . es) key
      (cond
	((and es (not (assoc e (trie-children trie))))
	 (setf-gettrie-1 nil e trie)
	 (setf (gettrie es (rest (assoc e (trie-children trie)))) value))
	(es (setf (gettrie es (rest (assoc e (trie-children trie)))) value))
	(t (setf-gettrie-1 value e trie))))))

(defun emptytrie () (make-trie))

(defun clrtrie (trie)
  (labels ((test (trie)
	     (and (not (trie-children trie))
		  (not (trie-value trie))))
	   (clrtrie-1 (trie) (clrtrie trie) trie))
    (setf (trie-children trie)
	  (remove-if #'test
	   (pairlis
	    (mapcar #'car (trie-children trie))
	    (mapcar #'clrtrie-1 (mapcar #'cdr (trie-children trie))))
	   :key #'cdr))))

(defun remtrie (key trie)
  (setf (gettrie key trie) nil)
  (clrtrie trie))

(defun copytrie (trie)
  (make-trie :value (trie-value trie)
	     :children
	     (pairlis
	      (mapcar #'car (trie-children trie))
	      (mapcar #'copytrie (mapcar #'cdr (trie-children trie))))))

(defun maptrie (func trie &optional &key maximum)
  (labels ((work (trie keys)
	     (when (and (trie-value trie) (or (not maximum) (null (trie-children trie))))
	       (funcall func (reverse keys) (trie-value trie)))
	     (dolist (child (trie-children trie))
	       (work (cdr child) (cons (car child) keys)))))
    (work trie nil)))

(defun counttrie (trie)
  (+
   (if (trie-value trie) 1 0)
   (if (trie-p trie)
       (apply #'+ (mapcar #'counttrie (mapcar #'cdr (trie-children trie))))
       1)))
      
(defun cleantrie (table)
  (maptrie (lambda (key value)
	     (if (zerop value) (remtrie key table))) table)
  (clrtrie table)
  table)

(defun settrie (key table value)
  (setf (gettrie key table) value)
  table)

(defun ensure-gettrie (key table default)
  (gettrie key table default))

(defun add (&rest ps)
  (let ((c (emptytrie)))
    (labels ((add-1 (key value)
	       (let ((value2 (+ (ensure-gettrie key c 0) value)))
		 (settrie key c value2))))
      (dolist (p ps) (maptrie #'add-1 p))
      (cleantrie c))))
(setf (fdefinition '+.) #'add)

(defun substruct (&rest ps)
  (let ((c (copytrie (car ps))))
    (labels ((substruct-1 (key value)
	       (let ((value2 (- (ensure-gettrie key c 0) value)))
		 (settrie key c value2))))
      (dolist (p (cdr ps)) (maptrie #'substruct-1 p))
      (cleantrie c))))
(setf (fdefinition '-.) #'substruct)

(defun multiply (&rest ps)
  (labels ((multiply-1 (a b)
	     (let ((c (emptytrie)))
	       (maptrie (lambda (key1 value1)
			  (maptrie (lambda (key2 value2)
				     (let* ((key3 (mapcar #'+ key1 key2))
					    (value3 (+ (ensure-gettrie key3 c 0)
						       (* value1 value2))))
				       (settrie key3 c value3)))
				   b))
			a)
	       (cleantrie c))))
    (reduce #'multiply-1 ps)))
(setf (fdefinition '*.) #'multiply)

(defun divide-monomial (&rest ps)
  (labels ((divide-1 (a b)
	     (let ((c (emptytrie)))
	       (maptrie (lambda (key1 value1)
			  (maptrie (lambda (key2 value2)
				     (let* ((key3 (mapcar #'- key1 key2))
					    (value3 (/ value1 value2)))
				       (settrie key3 c value3)))
				   b))
			a)
	       (cleantrie c))))
    (reduce #'divide-1 ps)))
(setf (fdefinition '/.) #'divide-monomial)

(defun lex (x y)
  (loop
     for a in x
     for b in y
     unless (= a b) return (< a b)))

(defun grlex (x y)
  (or (< (reduce #'+ x) (reduce #'+ y))
      (and 
       (= (reduce #'+ x) (reduce #'+ y))
       (lex x y))))

(defun grevlex (x y)
  (or (< (reduce #'+ x) (reduce #'+ y))
      (and 
       (= (reduce #'+ x) (reduce #'+ y))
       (lex (reverse x) (reverse y)))))

(defparameter *order* #'lex)

(defun leading-term (p)
  (let ((lt (emptytrie)) (lc 0) (lm nil))
    (maptrie (lambda (key value)
	       (if (or (not lm) (funcall *order* lm key))
		   (setf lc value lm key)))
	     p)
    (settrie lm lt lc)))
(setf (fdefinition 'lt) #'leading-term)

(defun leading-monomial (p)
  (let ((lt (emptytrie)) (lc 0) (lm nil))
    (maptrie (lambda (key value)
	       (if (or (not lm) (funcall *order* lm key))
		   (setf lc value lm key)))
	     p)
    (settrie lm lt 1)))
(setf (fdefinition 'lm) #'leading-monomial)

(defun leading-coefficient (p)
  (let ((lt (emptytrie)) (lc 0) (lm nil))
    (maptrie (lambda (key value)
	       (if (or (not lm) (funcall *order* lm key))
		   (setf lc value lm key)))
	     p)
    (settrie nil lt lc)))
(setf (fdefinition 'lc) #'leading-coefficient)

(defun multi-degree (p)
  (let (deg)
    (maptrie (lambda (key value)
	       (declare (ignore value))
	       (setf deg key))
	     p)
    deg))
(setf (fdefinition 'mdeg) #'multi-degree)

(defvar *variables* "xyz")

(defun parse-factor (s)
  (let ((a (emptytrie))
	(b (make-list (length *variables*) :initial-element 0)))
    (cond
      ((char= #\+ (char s 0))
       (parse-factor (subseq s 1)))
      ((char= #\- (char s 0))
       (let ((f (parse-factor (subseq s 1))))
	 (maptrie (lambda (key value) (settrie key f (- value))) f)
	 f))
      ((digit-char-p (char s 0))
       (settrie b a (parse-integer s)))
      ((and (position (char s 0) *variables*) (position #\^ s))
       (setf (nth (position (char s 0) *variables*) b)
	     (parse-integer (subseq s (1+ (position #\^ s)))))
       (settrie b a 1))
      (t
       (setf (nth (position (char s 0) *variables*) b) 1)
       (settrie b a 1)))))

(defun parse-term (s)
  (loop
     with d = ""
     for c across s
     if (char= c #\*) collecting
       (parse-factor d) into results and do
       (setq d "")
     else do
       (setf d (format nil "~a~a" d c))
     finally
       (return (apply #'*. (parse-factor d) results))))

(defun parse (s)
  (loop
     with d = ""
     for c across s
     if (and (not (string= d "")) (or (char= c #\+) (char= c #\-))) collecting
       (parse-term d) into results and do
       (setf d (format nil "~a" c))
     else do
       (setf d (format nil "~a~a" d c))
     finally
       (return (apply #'+. (parse-term d) results))))

(defun zero-polynomial-p (p)
  (zerop (counttrie p)))

(defun display (p)
  (when (zerop (counttrie p))
    (return-from display "0"))
  (let ((s (make-string-output-stream)))
    (maptrie (lambda (key value)
	       (cond
		 ((and (< 0 value) (every #'zerop key)) (format s "+~a" value))
		 ((every #'zerop key) (format s "~a" value))
		 ((= 1 value) (format s "+"))
		 ((= -1 value) (format s "-"))
		 ((> 0 value) (format s "~a" value))
		 ((< 0 value)(format s "+~a" value)))
	       (loop
		  with f = t
		  for i from 0
		  for e in key
		  unless (zerop e)
		  if (or (not f) (not (= 1 (* value value)))) do
		    (format s "*") 
		  else do
		    (setf f nil)
		  end and
		  if (= 1 e) do
		    (format s "~a" (char *variables* i))
		  else do
		    (format s "~a^~a" (char *variables* i) e)))
	     p)
    (let ((e (get-output-stream-string s)))
      (if (char= #\+ (char e 0))
	  (subseq e 1)
	  e))))

(defun divide (f &rest gs)
  (loop
     with s = (length gs)  and r = (emptytrie) and p = f
     with qs = (loop repeat s collect (emptytrie))
     for i = 0 and divisionoccurred = nil until (zero-polynomial-p p) do
       (loop
	  while (and (< i s) (not divisionoccurred))
	  for q = (nth i qs) and g = (nth i gs)
	  if (not (some #'minusp (mapcar #'- (mdeg (lt p)) (mdeg (lt g))))) do
	    (setf (nth i qs) (+. q (/. (lt p) (lt g)))
		  p (-. p (*. (/. (lt p) (lt g)) g))
		  divisionoccurred t)
	  else do
	    (incf i))
     unless divisionoccurred do
       (setf r (add r (leading-term p))
	     p (substruct p (leading-term p)))
     finally (return (values qs r))))

(defun least-common-multiple (a b)
  (let ((c (emptytrie)))
    (maptrie (lambda (key1 value1)
	       (maptrie (lambda (key2 value2)
			  (let* ((key3 (mapcar #'max key1 key2))
				 (value3 (lcm value1 value2)))
			    (settrie key3 c value3)))
			b))
	     a)
    c))
(setf (fdefinition 'lcm.) #'least-common-multiple)

(defun syzyxy-polynomial (f g)
  (-.
   (*. (/. (lcm. (lm f) (lm g)) (lt f)) f)
   (*. (/. (lcm. (lm f) (lm g)) (lt g)) g)))

(defun pairs (&rest ls)
  (loop
     for i from 0 below (1- (length ls)) append
       (loop
	  for j from (1+ i) below (length ls) collect
	    (cons (nth i ls) (nth j ls)))))

(defun groebner (&rest ideals)
  (loop
     with g = (mapcar #'parse ideals)
     for gp = g do
       (loop
	  for (p . q) in (apply #'pairs gp)
	  for (_ sm) = (multiple-value-list (apply #'divide (syzyxy-polynomial p q) gp))
	  unless (zero-polynomial-p sm) do
	    (pushnew sm g :test #'equalp))
     when (equalp g gp) return (mapcar #'display g)))


(mapc #'display (groebner (parse "x") (parse "y-x")))
