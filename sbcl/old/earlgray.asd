(in-package :cl)
(defpackage :earlgray.asd
  (:use :cl :asdf))
(in-package :earlgray.asd)

(defsystem earlgray
  :serial t
  :depends-on (:alexandria)
  :components ((:file "debug")
	       (:file "prime")
	       (:file "operator")
	       (:file "monomial")
	       (:file "polynomial")
	       (:file "parser")))
