(in-package :cl)
(defpackage :earlgray.polynomial
  (:use :cl :earlgray.monomial :earlgray.operator)
  (:import-from :alexandria
		compose
		curry
		length=)
  (:export make-polynomial
	   polynomial-terms
	   leading-term lt
	   divisiblep))
(in-package :earlgray.polynomial)

;;
;; Definition
;;

(defstruct (polynomial (:print-object print-polynomial))
  (terms nil))

(defun print-term (term stream)
  "This function displays a term, and is used in displaying funciton for polynomials."
  (destructuring-bind (coefficient . monomial) term
    (if (every #'zerop (monomial-exponents monomial))
	(format stream "~a" coefficient)
	(if (= 1 (* coefficient coefficient))
	    (format stream "~:[~;-~]~a" (minusp coefficient) monomial)
	    (format stream "~a*~a" coefficient monomial)))))

(defun print-polynomial (polynomial stream)
  "This function displays a polynomial."
  (let ((terms (polynomial-terms polynomial)))
    (format stream "$")
    (if (remove 0 terms :key #'car)
	(print-term (car terms) stream)
	(format stream "0"))
    (dolist (term (cdr terms))
      (when (plusp (car term))
	(format stream "+"))
      (unless (zerop (car term))
	(print-term term stream)))
    (format stream "$")))

;;
;; Utilities
;;

(defmethod leading-term ((polynomial polynomial) &optional (comparator #'lexicographical-order))
  (labels ((term-max (a b) (if (funcall comparator (cdr a) (cdr b)) b a)))
    (let ((term (reduce #'term-max (polynomial-terms polynomial))))
      (make-polynomial :terms (list term)))))

(setf (fdefinition 'lt) #'leading-term)

(defmethod divisiblep ((a polynomial) (b polynomial))
  (let ((a-terms (polynomial-terms a))
	(b-terms (polynomial-terms b)))
    (when (= 1 (* (length a-terms) (length b-terms)))
      (loop
	 :with terms := (polynomial-terms (divide a b))
	 :with exponents := (monomial-exponents (cdar terms))
	 :for e :across exponents :never (minusp e)))))

(defmacro aif (test then else)
  `(let ((it ,test))
     (if it ,then ,else)))

;;
;; Addition
;;

(defmethod add ((a polynomial) (b polynomial))
  (let (terms)
    (dolist (term (append (polynomial-terms a) (polynomial-terms b))
	     (make-polynomial :terms (remove 0 terms :key #'car)))
      (aif (rassoc (cdr term) terms :test #'equalp)
	   (incf (car it) (car term))
	   (push term terms)))))

(defmethod add ((a polynomial) (b number))
  (add a (make-polynomial :terms (list (cons b (make-monomial))))))

(defmethod add ((a number) (b polynomial))
  (add (make-polynomial :terms (list (cons a (make-monomial)))) b))

(defmethod add ((a number) (b number))
  (make-polynomial :terms (list (cons (+ a b) (make-monomial)))))

;;
;; Sustruction
;;

(defmethod substruct ((a polynomial) (b polynomial))
  (labels ((minus (term) (cons (- (car term)) (cdr term))))
    (let ((c (make-polynomial :terms (mapcar #'minus (polynomial-terms b)))))
      (add a c))))

(defmethod substruct ((a polynomial) (b number))
  (substruct a (make-polynomial :terms (list (cons b (make-monomial))))))

(defmethod substruct ((a number) (b polynomial))
  (substruct (make-polynomial :terms (list (cons a (make-monomial)))) b))

(defmethod substruct ((a number) (b number))
  (make-polynomial :terms (list (cons (- a b) (make-monomial)))))

;;
;; Multiplication
;;

(defmethod multiply ((a polynomial) (b polynomial))
  (let ((c (make-polynomial)))
    (dolist (a-term (polynomial-terms a) c)
      (dolist (b-term (polynomial-terms b))
	(let* ((d-term (cons (* (car a-term) (car b-term)) (multiply (cdr a-term) (cdr b-term))))
	       (d (make-polynomial :terms (list d-term))))
	  (setf c (add c d)))))))

(defmethod multiply ((a polynomial) (b number))
  (multiply a (make-polynomial :terms (list (cons b (make-monomial))))))

(defmethod multiply ((a number) (b polynomial))
  (multiply (make-polynomial :terms (list (cons a (make-monomial)))) b))

(defmethod multiply ((a number) (b number))
  (make-polynomial :terms (list (cons (* a b) (make-monomial)))))

;;
;; Division
;;

(defmethod divide ((a polynomial) (b polynomial))
  (let ((a-terms (polynomial-terms a))
	(b-terms (polynomial-terms b)))
    (if (= 1 (* (length a-terms) (length b-terms)))
      (let ((c-term (cons (/ (caar a-terms) (caar b-terms)) (divide (cdar a-terms) (cdar b-terms)))))
	(make-polynomial :terms (list c-term)))
      (cond ((not (= 1 (length a-terms)))
	     (error "~S is not a term." a))
	    ((not (= 1 (length b-terms)))
	     (error "~S is not a term." b))))))

;; Power

(defmacro -> (&body body) `(let (<>) ,@(mapcar (lambda (exp) `(setq <> ,exp)) body)))

(defmethod power ((p polynomial) (n number))
  (let* (terms
	 (m (length (polynomial-terms p)))
	 (f (make-array (1+ n) :initial-element 1)))
    (do ((i 1 (1+ i))) ((> i n))
      (setf (svref f i) (* i (svref f (1- i)))))
    (labels ((power-2 (a sum len)
	       (let* ((exponents
		       (cons (- n sum) a))
		      (coefficient
		       (* (-> exponents
			      (mapcar (curry #'svref f) <>)
			      (cons (svref f n) <>)
			      (apply #'/ <>))
			  (-> p
			      (polynomial-terms <>)
			      (mapcar #'car <>)
			      (mapcar #'expt <> exponents)
			      (apply #'* <>))))
		      (monomial
		       (-> p
			   (polynomial-terms <>)
			   (mapcar #'cdr <>)
			   (mapcar #'power <> exponents)
			   (reduce #'multiply <>))))
		 (push (cons coefficient monomial) terms)))
	     (power-1 (a sum len)
	       (if (= len (1- m))
		   (power-2 a sum len)
		   (dotimes (k (- n sum -1))
		     (power-1 (cons k a) (+ sum k) (1+ len))))))
      (power-1 nil 0 0))
    (make-polynomial :terms (remove 0 terms :key #'car))))
