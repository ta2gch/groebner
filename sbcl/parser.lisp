(in-package :cl)
(defpackage :groebner.parser
  (:use :cl :esrap)
  (:export pparse))
(in-package :groebner.parser)

(defrule zero #\0 (:lambda (_) (declare (ignore _)) 0))

(defrule natural
  (and (or #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
       (* (or #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)))
  (:lambda (ls) (parse-integer (format nil "~A~{~A~}" (first ls) (second ls)))))

(defrule integer (or zero natural))

(defrule rational 
  (and integer #\/ integer)
  (:lambda (ls) (/ (first ls) (third ls))))

(defrule number (or rational integer))

(defrule monomial-1
  (and character (? (and #\^ integer)))
  (:lambda (ls)
    (cons (intern (format nil "~@(~a~)" (first ls)))
	  (or (and (second ls) (second (second ls))) 1))))

(defrule monomial
  (and monomial-1 (* (and #\* monomial-1)))
  (:lambda (ls)
    (cons (first ls) (mapcar #'second (second ls)))))

(defrule term-1
  (and number #\*  monomial)
  (:lambda (ls) (cons (first ls) (third ls))))

(defrule term
  (or term-1 number monomial)
  (:lambda (ls)
    (cond ((numberp ls) (cons ls nil))
          ((consp (first ls)) (cons 1 ls))
          (t ls))))

(defrule polynomial-1
  (and (? (or #\+ #\-)) term)
  (:lambda (ls)
    (if (and (car ls) (string= "-" (first ls)))
        (cons (- (car (second ls))) (cdr (second ls)))
        (second ls))))

(defrule polynomial (+ polynomial-1))

(defun pparse (s) (parse 'polynomial s))
