(in-package :cl)
(require :groebner)
(defpackage :groebner.test
  (:use :cl :groebner))
(in-package :groebner.test)

(defparameter p (pparse "1+x"))
(defparameter q (pparse "1+x+y"))
(pprinc p)(princ " and ")(pprinc q)(terpri)

(princ "add ")(pprinc (p+ p q))(terpri)
(princ "sub ")(pprinc (p- p q))(terpri)
(princ "mul ")(pprinc (p* p q))(terpri)
